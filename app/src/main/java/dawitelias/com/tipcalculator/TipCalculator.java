package dawitelias.com.tipcalculator;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class TipCalculator extends ActionBarActivity {

    private static final String BILL_TOTAL = "BILL_TOTAL";
    private static final String CUSTOM_PERCENT = "CUSTOM_PERCENT";

    private double currentBillTotal;
    private int currentCustomPercent; // has to be an int to match SeekBar
    private EditText tip10EditText;
    private EditText total10EditText;
    private EditText tip15EditText;
    private EditText total15EditText;
    private EditText tip20EditText;
    private EditText total20EditText;
    private EditText billEditText;
    private TextView customTipTextView;
    private EditText tipCustomEditText;
    private EditText totalCustomEditText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tip_calculator);

        // check if app just started or being restored
        if (savedInstanceState == null) { // if null, just started
            currentBillTotal = 0.0;
            currentCustomPercent = 18;
        } else { // being restored from memory (not executing from scratch
            currentBillTotal = savedInstanceState.getDouble(BILL_TOTAL);
            currentCustomPercent = savedInstanceState.getInt(CUSTOM_PERCENT);
        }

        // get references to the EditTexts and TextView(s)
        // it's getting the view from the layout assigned in "setContentView()" above
        tip10EditText = (EditText)findViewById(R.id.tip10EditText);
        total10EditText = (EditText)findViewById(R.id.total10EditText);
        tip15EditText = (EditText)findViewById(R.id.tip15EditText);
        total15EditText = (EditText)findViewById(R.id.total15EditText);
        tip20EditText = (EditText)findViewById(R.id.tip20EditText);
        total20EditText = (EditText)findViewById(R.id.total20EditText);
        customTipTextView = (TextView)findViewById(R.id.customTipTextView);
        tipCustomEditText = (EditText)findViewById(R.id.tipCustomEditText);
        totalCustomEditText = (EditText)findViewById(R.id.totalCustomEditText);
        billEditText = (EditText)findViewById(R.id.billEditText);

        // handle onTextChanged event
        billEditText.addTextChangedListener(billEditTextWatcher);

        SeekBar customSeekBar = (SeekBar)findViewById(R.id.customSeekBar);
        customSeekBar.setOnSeekBarChangeListener(customSeekBarListener);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tip_calculator, menu);
        return true;
    }


    // update 10, 15 and 20 percent tip EditTexts
    private void updateStandard() {
        // calculate total with 10% tip
        double tenPercentTip = currentBillTotal * 0.1;
        double tenPercentTotal = currentBillTotal + tenPercentTip;
        tip10EditText.setText(String.format("%.02f", tenPercentTip)); // 2 decimal places float
        total10EditText.setText(String.format("%.02f", tenPercentTotal));

        // calculate total with 15% tip
        double fifteenPercentTip = currentBillTotal * 0.15;
        double fifteenPercentTotal = currentBillTotal + fifteenPercentTip;
        tip15EditText.setText(String.format("%.02f", fifteenPercentTip));
        total15EditText.setText(String.format("%.02f", fifteenPercentTotal));

        // calculate total with 20% tip
        double twentyPercentTip = currentBillTotal * 0.2;
        double twentyPercentTotal = currentBillTotal + twentyPercentTip;
        tip20EditText.setText(String.format("%.02f", twentyPercentTip));
        total20EditText.setText(String.format("%.02f", twentyPercentTotal));

    } // update standard

    // update custom tip and total
    private void updateCustom() {
        // match the position of SeekBar
        customTipTextView.setText(currentCustomPercent + "%");

        // calculate
        double customPercentTip = currentBillTotal * currentCustomPercent * 0.01;
        double customTotalAmount = currentBillTotal + customPercentTip;
        tipCustomEditText.setText(String.format("%.02f", customPercentTip));
        totalCustomEditText.setText(String.format("%.02f", customTotalAmount));

    } // update custom

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState); // inherit from parent class
        outState.putDouble(BILL_TOTAL, currentBillTotal);
        outState.putDouble(CUSTOM_PERCENT, currentCustomPercent);
    }

    private OnSeekBarChangeListener customSeekBarListener = new OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            // sets currentCustomPercent to seekbar position
            currentCustomPercent = seekBar.getProgress();
            // above line return integer 0-100
            updateCustom();
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };


    private TextWatcher billEditTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            // convert billEditText text to double
            // only using parameter "s" which contains a copy of the text
            // the other parameters indicate "count" characters starting at "start"
            // replaced previous text of length before
            try {
                currentBillTotal = Double.parseDouble(s.toString());
            } catch (NumberFormatException e) {
                currentBillTotal = 0.0;
            }
            updateStandard();
            updateCustom();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };
}
